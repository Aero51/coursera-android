package com.example.modernartuilab;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

public class MainActivity extends Activity {

	private int firstNonWhiteStartColor, firstNonWhiteEndColor;
	private int secondNonWhiteStartColor, secondNonWhiteEndColor;
	private int thirdNonWhiteStartColor, thirdNonWhiteEndColor;
	private int fourthNonWhiteStartColor, fourthNonWhiteEndColor;

	private View firstNonWhiteView;
	private View secondNonWhiteView;
	private View thirdNonWhiteView;
	private View fourthNonWhiteView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		firstNonWhiteStartColor = getResources().getColor(
				R.color.first_non_white_start_view_color);
		firstNonWhiteEndColor = getResources().getColor(
				R.color.first_non_white_end_view_color);

		secondNonWhiteStartColor = getResources().getColor(
				R.color.second_non_white_start_view_color);
		secondNonWhiteEndColor = getResources().getColor(
				R.color.second_non_white_end_view_color);

		thirdNonWhiteStartColor = getResources().getColor(
				R.color.third_non_white_start_view_color);
		thirdNonWhiteEndColor = getResources().getColor(
				R.color.third_non_white_end_view_color);

		fourthNonWhiteStartColor = getResources().getColor(
				R.color.fourth_non_white_start_view_color);
		fourthNonWhiteEndColor = getResources().getColor(
				R.color.fourth_non_white_end_view_color);

		firstNonWhiteView = findViewById(R.id.first_non_white_view);
		secondNonWhiteView = findViewById(R.id.second_non_white_view);
		thirdNonWhiteView = findViewById(R.id.third_non_white_view);
		fourthNonWhiteView = findViewById(R.id.fourth_non_white_view);

		SeekBar seek_bar = (SeekBar) findViewById(R.id.seek_bar);
		seek_bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {

		
				firstNonWhiteView.setBackgroundColor(updateColor(
						firstNonWhiteStartColor, firstNonWhiteEndColor,
						progress));
				secondNonWhiteView.setBackgroundColor(updateColor(
						secondNonWhiteStartColor, secondNonWhiteEndColor,
						progress));
				thirdNonWhiteView.setBackgroundColor(updateColor(
						thirdNonWhiteStartColor, thirdNonWhiteEndColor,
						progress));
				fourthNonWhiteView.setBackgroundColor(updateColor(
						fourthNonWhiteStartColor, fourthNonWhiteEndColor,
						progress));

			
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.more_information) {

			AlertDialog alertDialog = new AlertDialog.Builder(this).create();
			alertDialog.setTitle("Inspired by Piet Mondrian & Ben Nicholson.");
			alertDialog.setMessage("Click below to learn more!");
			alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE,
					getString(R.string.dialog_button_not_now),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
						}
					});

			alertDialog.setButton(DialogInterface.BUTTON_POSITIVE,
					getString(R.string.dialog_button_visit_moma),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {

							Intent browserIntent = new Intent(
									Intent.ACTION_VIEW, Uri
											.parse("http://www.moma.org"));
							startActivity(browserIntent);
						}
					});

			alertDialog.show();

			TextView textView = ((TextView) alertDialog
					.findViewById(android.R.id.message));

			if (textView != null)
				textView.setGravity(Gravity.CENTER);

			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private int updateColor(int color1, int color2, int progress) {

		int red, green, blue; // rgb components
		int deltaR, deltaG, deltaB; // change in rgb components

		deltaR = Color.red(color2) - Color.red(color1);
		deltaG = Color.green(color2) - Color.green(color1);
		deltaB = Color.blue(color2) - Color.blue(color1);

		red = Color.red(color1) + (deltaR * progress / 100);
		green = Color.green(color1) + (deltaG * progress / 100);
		blue = Color.blue(color1) + (deltaB * progress / 100);

		return Color.rgb(red, green, blue);

	}

}
