Coursera Programming Mobile Applications for Android Handheld Systems
==================================================

Lab projects with test cases for  Coursera's Programming Mobile Applications for Android Handheld Systems: Part 1


##References: 

[Coursera Mobile Cloud Computing with Android](https://www.coursera.org/specialization/mobilecloudcomputing2/36)

[Coursera Programming Mobile Applications for Android Handheld Systems: Part 1](https://class.coursera.org/androidpart1-001)




